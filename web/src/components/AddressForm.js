import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

export default function AddressForm({setLat, setLong}) {

    function handleLongChang(event) {
        setLong(event.target.value)
    }
    function handleLatChang(event) {
        setLat(event.target.value)
    }
    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                House address
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="address1"
                        name="address1"
                        label="Longitude"
                        fullWidth
                        autoComplete="shipping address-line1"
                        onChange={handleLongChang}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        id="address2"
                        name="address2"
                        label="Latitude"
                        fullWidth
                        autoComplete="shipping address-line2"
                        onChange={handleLatChang}
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    );
}