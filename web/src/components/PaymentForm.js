import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';

export default function PaymentForm({setSquare, setBathRoom, setBuilding_structure, setCommunity_average, setDrawing_room, setElevator, setKitchen, setLiving_room, setRenovation_condition}) {
    const [renovationCondition, setRenovationCondition] = React.useState('');
    const [buildingStructure, setBuildingStructure] = React.useState('');
    const [elevator, setIsElevator] = React.useState('');

    const handleChangeRenovationCondition = (event) => {
        setRenovationCondition(event.target.value);
        setRenovation_condition(event.target.value);
    };
    const handleChangeBuildingStructure = (event) => {
        setBuildingStructure(event.target.value);
        setBuilding_structure(event.target.value);
    };
    const handleChangeElevator = (event) => {
        setIsElevator(event.target.value);
        setElevator(event.target.value);
    };
    const handleChangeSquare = (event) => {
        setSquare(event.target.value);
    };
    const handleChangeBathRoom = (event) => {
        setBathRoom(event.target.value);
    };
    const handleChangeCommunity_average = (event) => {
        setCommunity_average(event.target.value);
    };
    const handleChangeDrawing_room = (event) => {
        setDrawing_room(event.target.value);
    };
    const handleChangeKitchen = (event) => {
        setKitchen(event.target.value);
    };
    const handleChangeLiving_room = (event) => {
        setLiving_room(event.target.value);
    };

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                House details
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="square"
                        label="Square"
                        helperText="The square of house"
                        type="number"
                        fullWidth
                        onChange={handleChangeSquare}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="livingRoom"
                        label="Living Room"
                        helperText="The number of living room"
                        type="number"
                        fullWidth
                        onChange={handleChangeLiving_room}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="bathroom"
                        label="Bathroom "
                        helperText="The number of bathroom"
                        type="number"
                        fullWidth
                        onChange={handleChangeBathRoom}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="drawingRoom"
                        label="Drawing Room"
                        helperText="The number of drawing room"
                        type="number"
                        fullWidth
                        onChange={handleChangeDrawing_room}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="kitchen"
                        label="Kitchen"
                        helperText="The number of kitchen"
                        type="number"
                        fullWidth
                        onChange={handleChangeKitchen}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <FormControl fullWidth required>
                        <InputLabel id="renovationConditionLabel">Renovation condition</InputLabel>
                        <Select
                            labelId="renovationConditionLabel"
                            id="renovationCondition"
                            value={renovationCondition}
                            onChange={handleChangeRenovationCondition}
                        >
                            <MenuItem value={1}>Other</MenuItem>
                            <MenuItem value={2}>Rough</MenuItem>
                            <MenuItem value={3}>Simplicity</MenuItem>
                            <MenuItem value={4}>Hardcover</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} md={6}>
                    <FormControl fullWidth required>
                        <InputLabel id="buildingStructureLabel">Building structure</InputLabel>
                        <Select
                            labelId="buildingStructureLabel"
                            id="buildingStructure"
                            value={buildingStructure}
                            onChange={handleChangeBuildingStructure}
                        >
                            <MenuItem value={1}>Unknow</MenuItem>
                            <MenuItem value={2}>Mixed</MenuItem>
                            <MenuItem value={3}>Brick and wood</MenuItem>
                            <MenuItem value={4}>Brick and concrete</MenuItem>
                            <MenuItem value={5}>Steel</MenuItem>
                            <MenuItem value={6}>Steel-concrete composite</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} md={6}>
                    <FormControl fullWidth required>
                        <InputLabel id="elevatorLabel">Elevator</InputLabel>
                        <Select
                            labelId="elevatorLabel"
                            id="elevator"
                            value={elevator}
                            onChange={handleChangeElevator}
                        >
                            <MenuItem value={1}>Yes</MenuItem>
                            <MenuItem value={0}>No</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}