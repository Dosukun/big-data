from fastapi import FastAPI
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware

from xgboost import XGBRegressor
import pandas as pd
import numpy as np

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI(debug=True)

origins = [
    "*"
]

app = CORSMiddleware(
    app=app,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.app.get("/")
async def main():
    print("test")
    return {"message": "Hello World"}


house = [
    {
        "id": "1",
        "price": "180000"
    },
]


@app.app.post("/house", tags=["house"])
async def get_house(house: dict) -> dict:
    print("test")
    print(house)
#   price = predict(house)
#   return {"data": price}


# square: the square of house
# livingRoom: the number of living room
# drawingRoom: the number of drawing room
# kitchen: the number of kitchen
# bathroom the number of bathroom
# floor: the height of the house. I will turn the Chinese characters to English in the next version.
# renovationCondition: including other( 1 ), rough( 2 ),Simplicity( 3 ), hardcover( 4 )
# buildingStructure: including unknown( 1 ), mixed( 2 ), brick and wood( 3 ), brick and concrete( 4 ),steel( 5 ) and steel-concrete composite ( 6 ).
# elevator have ( 1 ) or not have elevator( 0 )

def predict(
        square=100,
        bath_room=1,
        living_room=1,
        drawing_room=1,
        kitchen=1,
        renovation_condition=3,
        building_structure=6,
        elevator=1,
        community_average=58955
):
    data = pd.DataFrame(
        {'square': [square], 'bathRoom': [bath_room], 'livingRoom': [living_room], 'drawingRoom': [drawing_room],
         'kitchen': [kitchen], 'renovationCondition': [renovation_condition], 'buildingStructure': [building_structure],
         'elevator': [elevator], 'communityAverage': [community_average]})

    pred = model.predict(data)
    return round(pred[0], 2)


df = pd.read_csv("../data/houses_data.csv", encoding='unicode_escape')


def convert_to_int(list_to_convert, local_df):
    for string in list_to_convert:
        local_df[string] = pd.to_numeric(df[string], errors='coerce')
        local_df[string].fillna(0)
        local_df[string] = df[string].replace(np.nan, 0)
        local_df[string] = df[string].round().astype('int64')


def drop_col(list_drop, df_to_drop):
    for string in list_drop:
        df_to_drop = df_to_drop.drop([string], axis=1)
    return df_to_drop


df.Lat = df.Lat * 1000000
df.Lng = df.Lng * 1000000
df.ladderRatio = df.ladderRatio * 1000
listToInt = ['communityAverage', 'Lng', 'Lat', 'totalPrice', 'square', 'livingRoom',
             'drawingRoom', 'bathRoom', 'ladderRatio', 'buildingType',
             'elevator', 'fiveYearsProperty', 'constructionTime']
listToDrop = ['subway', 'tradeTime', 'DOM', 'followers', 'url', 'floor', "id"]
convert_to_int(listToInt, df)
df = drop_col(listToDrop, df)

features = ['square', 'bathRoom', 'livingRoom', 'drawingRoom', 'kitchen', 'renovationCondition', 'buildingStructure',
            'elevator', 'communityAverage']

X = df[features]
y = df['price']

# model = XGBRegressor(n_estimators=500)
# model.fit(X, y)
print("Machine learning trained.")
