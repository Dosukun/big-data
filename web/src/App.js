import React, { useState } from 'react';
import MainPage from './components/MainPage';
import { Route, BrowserRouter as Router } from 'react-router-dom'

function App() {


  return (
        <Router>
          <Route exact path="/" component={MainPage}/>
        </Router>
  );
}

export default App;