# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import pandas as pd
import numpy as np
import matplotlib as mtpip
import seaborn as sb
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
    # Device Name
    print("DeviceName: "+tf.test.gpu_device_name())
    # Version-check
    print("Version: "+tf.__version__)
    # CUDA Support
    print("CUDA Support: " + str(tf.test.is_built_with_cuda()))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('BigData')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
