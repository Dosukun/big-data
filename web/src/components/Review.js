import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import axios from 'axios'


const useStyles = makeStyles((theme) => ({
    listItem: {
        padding: theme.spacing(1, 0),
    },
    total: {
        fontWeight: 700,
    },
    title: {
        marginTop: theme.spacing(2),
    },
}));

export default function Review({finalForm}) {

    const classes = useStyles();
    const [price, setPrice] = React.useState(0);
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'Allow'},
        body: JSON.stringify(finalForm)
    };

    const fetchTodos = async () => {
        const response = await fetch("http://localhost:8080/house", requestOptions)
        const todos = await response.json()
        setPrice(todos.data)
    }
    useEffect(() => {
        fetchTodos()
    }, [])

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Price estimation :
            </Typography>
            <Typography variant="h6" gutterBottom>
                {price}
            </Typography>
        </React.Fragment>
    );
}