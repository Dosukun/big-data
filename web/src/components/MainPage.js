import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import AddressForm from './AddressForm';
import PaymentForm from './PaymentForm';
import Review from './Review';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    stepper: {
        padding: theme.spacing(3, 0, 5),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
}));

const steps = ['House address', 'House details', 'Result'];


export default function MainPage() {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const [long, setLong] = React.useState(0);
    const [lat, setLat] = React.useState(0);
    const [square, setSquare] = React.useState(0);
    const [bath_room, setBathRoom] = React.useState(0);
    const [living_room, setLiving_room] = React.useState(0);
    const [drawing_room, setDrawing_room] = React.useState(0);
    const [kitchen, setKitchen] = React.useState(0);
    const [renovation_condition, setRenovation_condition] = React.useState(0);
    const [building_structure, setBuilding_structure] = React.useState(0);
    const [elevator, setElevator] = React.useState(0);
    const [community_average, setCommunity_average] = React.useState(0);
    const [finalForm, setFinalForm] = React.useState([]);

    function getStepContent(step) {
        switch (step) {
            case 0:
                return <AddressForm setLong={setLong} setLat={setLat}/>;
            case 1:
                return <PaymentForm setSquare={setSquare} setBathRoom={setBathRoom} setLiving_room={setLiving_room}
                                    setDrawing_room={setDrawing_room} setKitchen={setKitchen} setRenovation_condition={setRenovation_condition}
                                    setBuilding_structure={setBuilding_structure} setElevator={setElevator} setCommunity_average={setCommunity_average}
                />;
            case 2:
                return <Review finalForm={finalForm}/>;
            default:
                throw new Error('Unknown step');
        }
    }


    useEffect(() => {
        setFinalForm({
            'square': square,
            'bath_room': bath_room,
            'living_room': living_room,
            'drawing_room': drawing_room,
            'kitchen': kitchen,
            'renovation_condition': renovation_condition,
            'building_structure' : building_structure,
            'elevator': elevator,
            'community_average': 58955,
        })
    }, [square, bath_room, living_room, drawing_room, kitchen, renovation_condition, building_structure,
        elevator, community_average])

    const handleNext = () => {
        setActiveStep(activeStep + 1);
    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
    };

    return (
        <React.Fragment>
            <CssBaseline/>
            <AppBar position="absolute" color="default" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" color="inherit" noWrap>
                        Beijing House Price Reviews
                    </Typography>
                </Toolbar>
            </AppBar>
            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography component="h1" variant="h4" align="center">
                        Simulation
                    </Typography>
                    <Stepper activeStep={activeStep} className={classes.stepper}>
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    <React.Fragment>
                        {activeStep === steps.length ? (
                            <React.Fragment>
                                <Typography variant="h5" gutterBottom>
                                    Thank you for your order.
                                </Typography>
                                <Typography variant="subtitle1">
                                    Your order number is #2001539. We have emailed your order confirmation, and will
                                    send you an update when your order has shipped.
                                </Typography>
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                {getStepContent(activeStep)}
                                <div className={classes.buttons}>
                                    {activeStep !== 0 && (
                                        <Button onClick={handleBack} className={classes.button}>
                                            Back
                                        </Button>
                                    )}
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={handleNext}
                                        className={classes.button}
                                    >
                                        {activeStep === steps.length - 1 ? 'Place order' : 'Next'}
                                    </Button>
                                </div>
                            </React.Fragment>
                        )}
                    </React.Fragment>
                </Paper>
                <Copyright/>
            </main>
        </React.Fragment>
    );
}